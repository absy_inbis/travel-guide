import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_guide/core/splash_screen.dart';
import 'package:travel_guide/features/destination/application/destination_bloc/destination_bloc.dart';
import 'package:travel_guide/features/destination/application/destination_bloc/destination_event.dart';
import 'package:travel_guide/features/destination/application/destination_bloc/destination_state.dart';
import 'package:travel_guide/features/destination/presentation/home_screen.dart';



class AppBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>BlocProvider(
    create: (_) =>DestinationBloc()..add(Home()),
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<DestinationBloc, DestinationState>(
        builder: (_,state) => state is Loaded ? HomeScreen(state.destinations) : SplachScreen(),
        ),
    ),
  );
}