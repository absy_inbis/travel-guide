import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_bloc.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_event.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

class Textfield extends StatefulWidget {
  Textfield(this.destinations);
  final List<Destination> destinations;
  @override
  _TextfieldState createState() => _TextfieldState();
}

class _TextfieldState extends State<Textfield> {
  GlobalKey<AutoCompleteTextFieldState<Destination>> key = GlobalKey();
  @override
  Widget build(BuildContext context) => AutoCompleteTextField<Destination>(
        key: key,
        controller: BlocProvider.of<SearchBloc>(context).search,
        suggestions: widget.destinations,
        itemFilter: (item, query) =>item.destinationName.toLowerCase().startsWith(query.toLowerCase()),
        itemSorter: (a, b) => a.destinationName.compareTo(b.destinationName),
        itemBuilder: (context, suggestion) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(suggestion.destinationName, style: TextStyle(fontSize: 15),),
        ),
        itemSubmitted: (data) => BlocProvider.of<SearchBloc>(context).search.text = data.destinationName,
        textSubmitted: (t) =>BlocProvider.of<SearchBloc>(context).add(Search()),
        clearOnSubmit: false,
        decoration: InputDecoration(
            border: InputBorder.none,
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey,
            ),
            hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
            hintText: "Search for cities, places ..."),
      );
}
