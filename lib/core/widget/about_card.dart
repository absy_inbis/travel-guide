import 'package:flutter/material.dart';

// This widget should recieve the Country about info and display it

class AboutCard extends StatelessWidget {
  const AboutCard(this.title, this.about);
  final String about, title;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 15,
      color: Colors.blue,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            color: Colors.blue,
            child: Text(
              title,
              style: TextStyle(fontSize: 35,color: Colors.white),
            ),
          ),
          SizedBox(height: 15),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
            ),
            padding: EdgeInsets.all(20),
            child: Text(
              about,
            ),
          ),
        ],
      ),
    );
  }
}
