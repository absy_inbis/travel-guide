import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class SplachScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplachScreen> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: FlareActor(
          "assets/travel.flr",
          alignment: Alignment.center,
          fit: BoxFit.fill,
          animation: "city-cloud",
        ),
      );
}
