import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_guide/core/animation/fade_animation.dart';
import 'package:travel_guide/core/widget/auto_complete_textfield.dart';
import 'package:travel_guide/core/widget/item_list.dart';
import 'package:travel_guide/core/widget/snackbar.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_bloc.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_event.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_state.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';
import 'package:travel_guide/features/destination/presentation/country_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen(this.destinations);
  final List<Destination> destinations;


  
  @override
  Widget build(BuildContext context) => BlocProvider(
    create: (_)=>SearchBloc(),
      child: Scaffold(
              body: BlocListener<SearchBloc,SearchState>(
                listener: (_,state) { 
                  if(state is NotFound){
                    Snackbar.showError(context);
                  }
                  if(state is Found){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=> CountryScreen(state.destination)),
                    );
                  } 
                },
                child: Builder(
                  builder: (context)=>SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * .5,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/background.jpg'),
                                  fit: BoxFit.cover),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                  gradient:
                                      LinearGradient(begin: Alignment.bottomRight, colors: [
                                Colors.black.withOpacity(.8),
                                Colors.black.withOpacity(.2),
                              ])),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  FadeAnimation(
                                      1,
                                      Text(
                                        "What you would like to find?",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 40,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  FadeAnimation(
                                      1.3,
                                      Container(
                                        padding: EdgeInsets.symmetric(vertical: 3),
                                        margin: EdgeInsets.symmetric(horizontal: 40),
                                        height: 50,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(50),
                                          color: Colors.white,
                                        ),
                                        child: Textfield(destinations),
                                      ),
                                      ),
                                  SizedBox(
                                    height: 30,
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                FadeAnimation(
                                    1,
                                    Text(
                                      "All Destinations",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey[800],
                                          fontSize: 20),
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                FadeAnimation(
                                    1.4,
                                    Container(
                                      height: 200,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: <Widget>[
                                          ...destinations.map((destination) => 
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context)=> CountryScreen(destination)),
                                              );
                                            },
                                            child: ItemList(image: destination.image,title: destination.destinationName),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    ),
                                SizedBox(
                                  height: 80,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                ), 
              ),
            ),
      );
}
