import 'package:flutter/material.dart';
import 'package:travel_guide/core/widget/about_card.dart';
import 'package:travel_guide/core/widget/item_list.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

class CountryScreen extends StatelessWidget {
  const CountryScreen(this.destination);
  final Destination destination;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) =>[
          SliverAppBar(
          expandedHeight: 400,
          floating: false,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Text(
              destination.destinationName,
              style: TextStyle(
                color: Colors.white,
                fontSize: 30,
              ),
            ),
            background: Image.network(
                destination.image,
                fit: BoxFit.cover,
              ),
          ),
        ),
        ],
        body: SingleChildScrollView(
                  child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 23,
              ),
              AboutCard(
                  'About',
                  destination
                      .about), // We should send the Country about info here as a parameter
              SizedBox(
                height: 32,
              ),
              SizedBox(
                height: 23,
              ),
              AboutCard(
                  'Tourist Place',
                  destination
                      .touristPlaces), // We should send the Country about info here as a parameter
              SizedBox(
                height: 32,
              ),
              SizedBox(
                height: 23,
              ),
              AboutCard(
                  'PopularFood',
                  destination
                      .popularFood), // We should send the Country about info here as a parameter
              SizedBox(
                height: 32,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Other picture of destination",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[800],
                      fontSize: 20),
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                height: 200,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    ...destination.otherImage
                        .split(',')
                        .map((link) => ItemList(image: link)),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
