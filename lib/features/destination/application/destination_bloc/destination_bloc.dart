import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_guide/features/destination/application/destination_bloc/destination_event.dart';
import 'package:travel_guide/features/destination/application/destination_bloc/destination_state.dart';
import 'package:travel_guide/features/destination/data/repositories/destination_repository_impl.dart';
import 'package:travel_guide/features/destination/domain/repositories/destination_repository.dart';


class DestinationBloc extends Bloc<DestinationEvent,DestinationState>{
  
  DestinationBloc() : super(Loading());

  DestinationRepository destinationRepository = DestinationRepositoryImpl();

  @override
  Stream<DestinationState> mapEventToState(DestinationEvent event) async* {
    if(event is Home) {
      yield Loaded(await destinationRepository.getDestinations());
    }
  }
}