import 'package:travel_guide/features/destination/domain/entities/destination.dart';

abstract class DestinationState {}

class Loading extends DestinationState {}

class Loaded extends DestinationState {
  Loaded(this.destinations);

  final List<Destination> destinations;
}

class NotFound extends DestinationState {}