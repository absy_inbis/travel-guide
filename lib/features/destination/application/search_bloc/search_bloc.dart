import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_event.dart';
import 'package:travel_guide/features/destination/application/search_bloc/search_state.dart';
import 'package:travel_guide/features/destination/data/repositories/destination_repository_impl.dart';
import 'package:travel_guide/features/destination/domain/repositories/destination_repository.dart';

class SearchBloc extends Bloc<SearchEvent,SearchState>{

  SearchBloc() : super(Intit());
  
  DestinationRepository destinationRepository = DestinationRepositoryImpl();
  TextEditingController search = TextEditingController();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async*  {
    try {
      print('absy ${search.text}');
      yield Found(await destinationRepository.getDestination(search.text.toLowerCase()));
      search.clear();
    } catch (e) {
      yield NotFound();
    }
  }

}