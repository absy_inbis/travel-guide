import 'package:travel_guide/features/destination/application/destination_bloc/destination_event.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

class SearchState {}

class Intit extends SearchState{}

class NotFound extends SearchState{}

class Found extends SearchState {
  Found(this.destination);
  Destination destination;
}