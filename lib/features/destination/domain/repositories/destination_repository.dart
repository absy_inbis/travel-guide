import 'package:travel_guide/features/destination/domain/entities/destination.dart';

abstract class DestinationRepository {
  const DestinationRepository();

  Future<List<Destination>> getDestinations();
  Future<Destination> getDestination(String destination);
}