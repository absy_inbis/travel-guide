import 'package:flutter/foundation.dart';

class Destination {
  const Destination({
    @required this.destinationName,
    @required this.about,
    @required this.touristPlaces,
    @required this.popularFood,
    @required this.image,
    @required this.otherImage,
  });

  final String destinationName,about,touristPlaces,popularFood,image,otherImage;


}