
import 'package:flutter/foundation.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

class DestinationModel extends Destination {
  DestinationModel({
    @required String destinationName,
    @required String about,
    @required String touristPlaces,
    @required String popularFood,
    @required String image,
    @required String otherImage,
  }): super(destinationName: destinationName,about: about ,touristPlaces: touristPlaces,popularFood: popularFood,image: image,otherImage: otherImage);

  DestinationModel.fromjson(Map<String , Object> json)
  : super(
    destinationName: json['destinationName'],
    about: json['about'] ,
    touristPlaces: json['touristPlaces'],
    popularFood: json['popularFood'],
    image: json['image'],
    otherImage: json['otherImage']
    );


}