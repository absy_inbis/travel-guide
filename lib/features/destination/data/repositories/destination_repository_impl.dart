
import 'package:travel_guide/features/destination/data/data_sourec/remote_data_source_imp.dart';
import 'package:travel_guide/features/destination/data/data_sourec/remote_data_sourec.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';
import 'package:travel_guide/features/destination/domain/repositories/destination_repository.dart';

class DestinationRepositoryImpl extends DestinationRepository {
  DestinationRepositoryImpl();

  RemoteDataSourec remoteDataSourec = RemoteDataSourecImpl();
  @override
  Future<Destination> getDestination(String destination) => remoteDataSourec.getDestination(destination);

  @override
  Future<List<Destination>> getDestinations() => remoteDataSourec.getDestinations(); 

}
