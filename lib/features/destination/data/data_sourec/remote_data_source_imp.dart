
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:travel_guide/features/destination/data/data_sourec/remote_data_sourec.dart';
import 'package:travel_guide/features/destination/data/model/destination_model.dart';
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

class RemoteDataSourecImpl extends RemoteDataSourec {
  @override
  Future<Destination> getDestination(String destination) async{

    final result = await Firestore.instance.collection('countries').document(destination).get();

      if(result != null)
        return DestinationModel.fromjson(result.data);
        else
        throw "not found";
  }
  
  @override
  Future<List<Destination>> getDestinations() async{
    await Future.delayed(Duration(seconds: 5));
    final data = await Firestore.instance.collection('countries').getDocuments();
    final list = data.documents.map<Destination>((destination) => DestinationModel.fromjson(destination.data)).toList();
    print(list);
    return list;
  }

}