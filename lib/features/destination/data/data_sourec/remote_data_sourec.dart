
import 'package:travel_guide/features/destination/domain/entities/destination.dart';

abstract class RemoteDataSourec {
  const RemoteDataSourec();

  Future<List<Destination>> getDestinations();
  Future<Destination> getDestination(String destination);
}